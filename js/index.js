var todoList = document.getElementById("newTask").value;
var listArray = [];
var doneArray = [];
const TODO_LOCALSTORAGE = "TODO_LOCALSTORAGE";
const DONE_LOCALSTORAGE = "DONE_LOCALSTORAGE";

const luuLocalStorage = function () {
  var todoJson = JSON.stringify(listArray);
  localStorage.setItem("TODO_LOCALSTORAGE", todoJson);
};

const doneLocalStorage = function () {
  var doneJson = JSON.stringify(doneArray);
  localStorage.setItem("DONE_LOCALSTORAGE", doneJson);
};
const addItem = () => {
  var todoList = document.getElementById("newTask").value;

  if (todoList.trim() != 0) {
    listArray.push(todoList);
    luuLocalStorage();

    var todoJson = localStorage.getItem(TODO_LOCALSTORAGE);
    listArray = JSON.parse(todoJson);
    console.log({ listArray });

    showList();
  } else {
    return alert("Không được để trống");
  }
};
const showList = () => {
  var todoJson = localStorage.getItem(TODO_LOCALSTORAGE);
  listArray = JSON.parse(todoJson);
  let newLiTag = "";
  listArray.forEach((element, index) => {
    newLiTag += `<li>${element} <div><span class="icon mr-3" onclick="doneTask(${index})"><i style="color:green" class="fa fa-check"></i></span><span class="icon" onclick="deleteTask(${index})"><i style="color:red" class="fas fa-trash"></i></span></div></li>`;
    console.log({ element });
  });
  todo.innerHTML = newLiTag;
  todoList.value = "";
};
const deleteTask = (index) => {
  var todoJson = localStorage.getItem(TODO_LOCALSTORAGE);
  listArray = JSON.parse(todoJson);
  listArray.splice(index, 1);
  luuLocalStorage();
  showList();
};

const deleteDoneTask = (index) => {
  var doneJson = localStorage.getItem(DONE_LOCALSTORAGE);
  doneArray = JSON.parse(doneJson);
  doneArray.splice(index, 1);
  console.log({ index });
  doneLocalStorage();
  listDoneTask();
};

const doneTask = (index) => {
  console.log({ index });
  var todoJson = localStorage.getItem(TODO_LOCALSTORAGE);
  listArray = JSON.parse(todoJson);
  doneList = listArray[index];
  doneArray.push(doneList);
  doneLocalStorage();
  listArray.splice(index, 1);
  luuLocalStorage();
  showList();
  listDoneTask();
};

const listDoneTask = () => {
  let contentHTML = "";
  var doneJson = localStorage.getItem(DONE_LOCALSTORAGE);
  let doneArray = JSON.parse(doneJson);
  for (i = 0; i < doneArray.length; i++) {
    let newLiTag = `<li id="haha">${doneArray[i]} <div><span class="mr-3" ><i class="fa mri fa-check-circle"></i></i></span><span class="icon" onclick="deleteDoneTask(${i})"><i style="color:red" class="fas fa-trash"></i></span></div></li>`;
    contentHTML += newLiTag;
  }
  completed.innerHTML = contentHTML;
};

document.querySelector("#two").addEventListener("click", () => {
  console.log("yes");
  listArray.sort((a, b) => {
    return a.localeCompare(b);
  });
  console.log({ listArray });
  luuLocalStorage();
  showList();
});

document.querySelector("#three").addEventListener("click", () => {
  console.log("No");
  listArray.sort((a, b) => {
    return b.localeCompare(a);
  });
  console.log({ listArray });
  luuLocalStorage();
  showList();
});
listDoneTask();
showList();
